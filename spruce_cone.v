// SPDX-License-Identifier: GPL-3.0-or-later
// spruce_cone: yet another Sony's GIM converter

import os
// import compress.zlib
import encoding.binary
import henrixounez.vpng

const zlib_signature   = [u8(0x78) 0x9C]
const gim_le_signature = [u8(0x4D) 0x49 0x47 0x2E 0x30 0x30 0x2E 0x31]
const gim_be_signature = [u8(0x2E) 0x47 0x49 0x4D 0x31 0x2E 0x30 0x30]

enum Endian {
	little
	big
}

struct Gim {
	pub mut:
		endian  Endian
		width   u16
		height  u16
		image   []u8
		palette []u8
}

struct Color {
	pub mut:
		red   u8
		green u8
		blue  u8
}

fn (c Color) str() string {
	return '#${c.red.hex()}${c.green.hex()}${c.blue.hex()}'
}

fn rgba5650_to_rgba8888(input []u8) []Color {
	mut output := []Color{}
	if input.len % 2 == 0 {
		for i := 0; i < input.len; i += 2 {
			rgba5650 := binary.little_endian_u16_at(input, i)
			// https://stackoverflow.com/a/9069480
			r5 := u16((rgba5650 & 0b0000000000011111) >> 0)
			g6 := u16((rgba5650 & 0b0000011111100000) >> 5)
			b5 := u16((rgba5650 & 0b1111100000000000) >> 11)
			r8 := u8((r5 * 527 + 23) >> 6)
			g8 := u8((g6 * 259 + 33) >> 6)
			b8 := u8((b5 * 527 + 23) >> 6)
			output << Color {
				red:   r8
				green: g8
				blue:  b8
			}
		}
	}
	return output
}

fn u8_to_indexed(input []u8) []vpng.Pixel {
	mut output := []vpng.Pixel{}
	for i in input {
		output << vpng.Pixel(vpng.Indexed {
			index: i
		})
	}
	return output
}

fn color_to_vpng(input []Color) []vpng.TrueColor {
	mut output := []vpng.TrueColor{}
	for color in input {
		output << vpng.TrueColor {
			red: color.red
			green: color.green
			blue: color.blue
		}
	}
	return output
}

fn interlace(input []u8, image_width int) []u8 {
	mut output := []u8{cap: input.len}
	cell_width := 16
	cell_height := 8
	crpq := cell_height * image_width
	t := cell_width * cell_height
	for i := 0; i < input.len; i += crpq {
		for j := 0; j < t; j += cell_width {
			for k := 0; k < crpq; k += t {
				output << input[i + k + j .. i + k + j + cell_width]
			}
		}
	}
	assert output.len == input.len
	return output
}

fn main() {
	mut input := []u8{}
	if os.args.len < 2 {
		input = os.get_raw_stdin()
	}
	// if input[..zlib_signature.len] == zlib_signature {
	// 	println('input is zlib-compressed')
	// 	input = zlib.decompress(input) or {
	// 		panic('failed to decompress input')
	// 	}
	// }
	mut input_parsed := Gim{}
	if input[..gim_le_signature.len] == gim_le_signature {
		println('input is little-endian gim image')
		input_parsed.endian = .little
		mut offset := u32(48)
		input_parsed.palette = input[offset + binary.little_endian_u32_at(input, int(offset + 0x08)) ..]
		input_parsed.image = input[offset .. offset + binary.little_endian_u32_at(input, int(offset + 0x04))]
	}

	image_width := binary.little_endian_u16_at(input_parsed.image, 0x18)
	output := vpng.PngFile {
		ihdr: vpng.IHDR{
			bit_depth: 8
			color_type: 3
		}
		width: image_width
		height: binary.little_endian_u16_at(input_parsed.image, 0x1A)
		pixel_type: .indexed
		palette: color_to_vpng(rgba5650_to_rgba8888(input_parsed.palette[80 .. input_parsed.palette.len]))
		pixels: u8_to_indexed(interlace(input_parsed.image[80 .. input_parsed.image.len], image_width))
	}
	output.write('test_vpng.png')
}